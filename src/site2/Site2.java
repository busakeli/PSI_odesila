/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package site2;

//ODESILA
import java.net.*;
import java.io.*;
import java.security.*;
import java.lang.*;
import java.util.concurrent.*;
import java.util.zip.*;

/**
 *
 * @author Eliška
 */
public class Site2 {

    CRC crcClass = new CRC();
    long crcLong = 0;

    String ServerIP;
    String ClientIP;
    InetAddress IpAddress;
    int ServerPort_SEND;
    int ClientPort_LISTEN;
    int ServerPort_LISTEN;
    int length = 1024; //delka packetu
    int counter = 0;
    String file;

    byte[] data;
    byte[] SaWfromClient;
    byte[] hash;
    byte[] crcByte;

    byte[] both2;
    byte[] stopAndWait;
    DatagramSocket serverSocket;
    boolean OK = false;

    public Site2() {
        ServerIP = "127.0.0.1";
        ClientIP = "127.0.0.1";
        ServerPort_SEND = 8888; //odkud posila server
        ClientPort_LISTEN = 1111; //kde posloucha client
        file = "C:\\Users\\Eliška\\Desktop\\CVUT\\APO\\projekt\\file.txt";
        data = new byte[length];
        SaWfromClient = new byte[1];
        crcClass = new CRC();
        both2 = new byte[length + 9];
        stopAndWait = new byte[1];

        try {
            IpAddress = InetAddress.getByName(ClientIP);
        } catch (UnknownHostException u) {
            u.printStackTrace();
        }

    }

    public Site2(String sIP, String cIP) {
        ServerIP = sIP;
        ClientIP = cIP;
        ServerPort_SEND = 8888;
        ClientPort_LISTEN = 1111;
        ServerPort_LISTEN = 8000;
        file = "C:\\Users\\Eliška\\Desktop\\CVUT\\APO\\projekt\\poslat.jpg";
        data = new byte[length];
        SaWfromClient = new byte[1];
        crcClass = new CRC();
        both2 = new byte[length + 9];
        stopAndWait = new byte[1];

        try {
            IpAddress = InetAddress.getByName(ClientIP);
        } catch (UnknownHostException u) {
            u.printStackTrace();
        }

    }

    private void getFileChecksum() throws IOException, NoSuchAlgorithmException {

        File file = new File(this.file);
        MessageDigest digest = MessageDigest.getInstance("MD5");
        FileInputStream fis = new FileInputStream(file);
        byte[] byteArray = new byte[1024];
        int bytesCount = 0;
        while ((bytesCount = fis.read(byteArray)) != -1) {
            digest.update(byteArray, 0, bytesCount);
        }

        fis.close();
        hash = digest.digest();
    }

    private long makeCRC(byte[] D, int I) {
        long Lcrc;
        Lcrc = crcClass.update(D, 0, I);
        crcClass.reset();
        return Lcrc;
    }

    private byte[] longToByte(long x) throws IOException {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(baos);
        dos.writeLong(x);
        dos.close();
        return baos.toByteArray();
    }

    private void openFile() throws FileNotFoundException, IOException, InterruptedException {

        FileInputStream fr = new FileInputStream(file);
        int c;
        boolean end = false;
        serverSocket = new DatagramSocket(ServerPort_SEND);

        while (!end) {
            c = fr.read(data);
            if (c == -1) {
                end = true;
            }

            OK = false;
            crcLong = crcClass.update(data, 0, length);
            crcClass.reset();

            crcByte = longToByte(crcLong);
            System.out.println("byte crc:" + crcByte);

            stopAndWait[0] = (byte) (counter); //prvni packet: 0

            if (c < length) {
                stopAndWait[0] = -1;
            } //konec prenosu

            for (int i = 0; i < (length + 9); i++) {
                if (i == 0) {
                    System.out.println("Stavim novy packet");
                }
                if (i == 0) {
                    both2[i] = stopAndWait[0];
                } else if (i < 9) {
                    both2[i] = crcByte[i - 1];
                } else {
                    both2[i] = data[i - 9];
                }
            }

            while (!OK) {
                if (SaWfromClient[0] == -2) {
                   break;
                }
                DatagramPacket packetToSend = new DatagramPacket(both2, c + 9, IpAddress, ClientPort_LISTEN);
                System.out.println("posilam SaW: " + stopAndWait[0]);
                System.out.println("posilam CRC: " + crcByte);
                TimeUnit.SECONDS.sleep(1);
                serverSocket.send(packetToSend);

                DatagramPacket receivePacket = new DatagramPacket(SaWfromClient, 1);
                serverSocket.receive(receivePacket);
                System.out.println("dorucen SaW: " + SaWfromClient[0]);

                if ((SaWfromClient[0] == -2)) {
                    OK = true;
                    System.out.println("Prenos byl uspesne ukoncen.");
                    break;
                } else if (SaWfromClient[0] == (byte) counter) {
                    OK = true;
                    counter++;
                } else {
                    System.out.printf("znovu posilam packer cislo: %d\n", counter);
                    serverSocket.send(packetToSend);
                }
            }

            data = new byte[length];

        }
        DatagramPacket packetToSend = new DatagramPacket(hash, hash.length, IpAddress, ClientPort_LISTEN);
        System.out.println("posilam hash: " + hash);
        System.out.println("delka hashe: " + hash.length);
        TimeUnit.SECONDS.sleep(2);
        serverSocket.send(packetToSend);

        serverSocket.close();

    }

    public static void main(String[] args) {
        // TODO code application logic here
        try {
            Site2 zkouska = new Site2();
            zkouska.getFileChecksum();
            zkouska.openFile();

        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

}
